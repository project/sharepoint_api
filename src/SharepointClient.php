<?php

namespace Drupal\sharepoint_api;

use GuzzleHttp\Client;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\DriveItem;

/**
 * The service we use to interact with the API.
 */
class SharepointClient {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Settings for the integration.
   *
   * @var array
   */
  private array $settings = [];

  /**
   * Constructs a SharepointClient object.
   */
  public function __construct(Client $http_client, $settings = []) {
    $this->httpClient = $http_client;
    $this->settings = $settings;
  }

  /**
   * Download a specific file by its share ID.
   */
  public function downloadFileByShareId($share_id, $destination) : void {
    $graph = $this->getGraph();
    $graph->createRequest('GET', sprintf('/shares/%s/driveItem/content', $share_id))
      ->download($destination);
  }

  /**
   * Get the drive item info from a share id.
   */
  public function getDriveItemByShareId($share_id) : ?DriveItem {
    $graph = $this->getGraph();
    $item = $graph->createRequest('GET', sprintf('/shares/%s/driveItem', $share_id))
      ->setReturnType(DriveItem::class)
      ->execute();
    if (!$item instanceof DriveItem) {
      return NULL;
    }
    return $item;
  }

  /**
   * Helper to get the graph.
   */
  public function getGraph() : Graph {
    $graph = new Graph();
    $access_token = $this->getAccessToken();
    $graph->setAccessToken($access_token);
    return $graph;
  }

  /**
   * Helper to get the access token.
   */
  public function getAccessToken() : string {
    $url = 'https://login.microsoftonline.com/' . $this->settings['tenant_id'] . '/oauth2/v2.0/token';
    $token = json_decode($this->httpClient->post($url, [
      'form_params' => [
        'client_id' => $this->settings['client_id'],
        'client_secret' => $this->settings['client_secret'],
        'scope' => 'https://graph.microsoft.com/.default',
        'grant_type' => 'client_credentials',
      ],
    ])->getBody()->getContents());
    $accessToken = $token->access_token;
    // @todo We could probably cache this a bit. Maybe as a setting?
    return $accessToken;
  }

}
