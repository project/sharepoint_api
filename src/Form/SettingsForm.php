<?php

namespace Drupal\sharepoint_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Sharepoint API settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sharepoint_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sharepoint_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $this->config('sharepoint_api.settings')->get('client_id'),
    ];
    $form['client_secret'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $this->config('sharepoint_api.settings')->get('client_secret'),
    ];
    $form['tenant_id'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Tenant ID'),
      '#default_value' => $this->config('sharepoint_api.settings')->get('tenant_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sharepoint_api.settings')
      // Take all the values from the build form and save them into config.
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('tenant_id', $form_state->getValue('tenant_id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
