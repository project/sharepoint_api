<?php

namespace Drupal\sharepoint_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;

/**
 * The client factory.
 */
class SharepointClientFactory {

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $client;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Construct the factory.
   */
  public function __construct(Client $client, ConfigFactoryInterface $configFactory) {
    $this->client = $client;
    $this->configFactory = $configFactory;
  }

  /**
   * The helper for getting the client.
   */
  public function getClient() : SharepointClient {
    $settings = $this->configFactory->get('sharepoint_api.settings');
    return new SharepointClient($this->client, [
      'client_id' => $settings->get('client_id'),
      'client_secret' => $settings->get('client_secret'),
      'tenant_id' => $settings->get('tenant_id'),
    ]);
  }

}
